import React from 'react';
import './ModeToggle.css';

const ModeToggle = props => {
  const modeConfig = {
    day: {
      icon: 'moon',
      btnStyle: 'black'
    },
    night: {
      icon: 'sun',
      btnStyle: 'inverted'
    }
  };

  return (
    <div className="mode-toggle ui buttons">
      <button
        className={`ui ${modeConfig[props.mode].btnStyle} toggle button`}
        onClick={() => {
          props.onModeToggle();
        }}
      >
        <i className={`icon ${modeConfig[props.mode].icon}`} />
      </button>
    </div>
  );
};

export default ModeToggle;
