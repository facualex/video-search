import React from 'react';
import Moment from 'react-moment';

const VideoDetail = ({ video, mode }) => {
  if (!video) {
    return <div>Loading...</div>;
  }

  const modeStyle = {
    night: {
      backgroundColor: '#3A3C40',
      color: '#fff'
    },
    day: {
      backgroundColor: '#fff',
      color: '#000000'
    }
  };

  const videoSrc = `https://youtube.com/embed/${video.id.videoId}`;

  const isLive =
    video.snippet.liveBroadcastContent === 'live' ? (
      <div>
        <h5
          className="ui right floated header"
          style={{ color: modeStyle[mode].color }}
        >
          <i
            className="icon circle"
            style={{ color: 'red', fontSize: '15px' }}
          />{' '}
          Now live
        </h5>
      </div>
    ) : null;

  return (
    <div>
      <div className="ui embed">
        <iframe title="video player" src={videoSrc} />
      </div>
      <div
        className="ui segment"
        style={{ backgroundColor: modeStyle[mode].backgroundColor }}
      >
        <div
          className="ui clearing segment"
          style={{ backgroundColor: modeStyle[mode].backgroundColor }}
        >
          <h4
            className="ui left floated header"
            style={{ color: modeStyle[mode].color }}
          >
            {video.snippet.title}
          </h4>
          {isLive}
          <p style={{ color: modeStyle[mode].color }}>
            {video.snippet.channelTitle}
          </p>
          <p style={{ color: modeStyle[mode].color }}>
            Uploaded at{' '}
            <Moment format="DD-MM-YYYY - HH:mm">
              {video.snippet.publishedAt}
            </Moment>{' '}
            hrs
          </p>
        </div>

        <p style={{ color: modeStyle[mode].color }}>
          {video.snippet.description}
        </p>
      </div>
    </div>
  );
};

export default VideoDetail;
