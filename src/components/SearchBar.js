import React from 'react';
import PropTypes from 'prop-types';

class SearchBar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      term: ''
    };
  }

  onInputChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  };

  onFormSubmit = event => {
    event.preventDefault();

    this.props.onFormSubmit(this.state.term);
  };

  render() {
    const modeStyle = {
      night: {
        backgroundColor: '#3A3C40',
        color: '#fff'
      },
      day: {
        backgroundColor: '#fff',
        color: '#000000'
      }
    };

    return (
      <div
        className="search-bar ui segment"
        style={{ backgroundColor: modeStyle[this.props.mode].backgroundColor }}
      >
        <form onSubmit={this.onFormSubmit} className="ui form">
          <div className="field">
            <label
              htmlFor="term"
              style={{ color: modeStyle[this.props.mode].color }}
            >
              Video Search
            </label>
            <input
              type="text"
              name="term"
              id="term"
              value={this.state.term}
              onChange={this.onInputChange}
            />
          </div>
        </form>
      </div>
    );
  }
}

SearchBar.propTypes = {
  onFormSubmit: PropTypes.func.isRequired
};

export default SearchBar;
