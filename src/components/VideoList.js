import React from 'react';
import PropTypes from 'prop-types';
import VideoItem from './VideoItem';

const VideoList = ({ videos, onVideoSelect, mode }) => {
  const renderedList = videos.map(video => {
    return (
      <VideoItem
        key={video.id.videoId}
        video={video}
        onVideoSelect={onVideoSelect}
        mode={mode}
      />
    );
  });

  return <div className="ui relaxed divided list"> {renderedList} </div>;
};

VideoList.propTypes = {
  onVideoSelect: PropTypes.func.isRequired,
  videos: PropTypes.array.isRequired
};

export default VideoList;
