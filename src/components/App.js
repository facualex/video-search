import React from 'react';
import youtube from '../api/youtube';

import SearchBar from './SearchBar';
import VideoList from './VideoList';
import VideoDetail from './VideoDetail';
import ModeToggle from './ModeToggle';

class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      videos: [],
      selectedVideo: null,
      mode: 'day'
    };

    this.ref = React.createRef();
  }

  componentDidMount() {
    this.onTermSubmit('lofi hip hop radio');
  }

  onTermSubmit = async term => {
    const response = await youtube.get('/search', {
      params: {
        q: term
      }
    });

    this.setState({
      videos: response.data.items,
      selectedVideo: response.data.items[0]
    });
  };

  onVideoSelect = video => {
    this.setState({ selectedVideo: video });
  };

  onModeToggle = () => {
    const toggle = this.state.mode === 'day' ? 'night' : 'day';
    const backgroundColor = toggle === 'night' ? '#2f3032' : null;

    this.setState({ mode: toggle });
    this.ref.current.offsetParent.style.background = backgroundColor;
  };

  render() {
    return (
      <div
        ref={this.ref}
        className="ui container"
        style={{ marginTop: '15px' }}
      >
        <ModeToggle onModeToggle={this.onModeToggle} mode={this.state.mode} />
        <SearchBar onFormSubmit={this.onTermSubmit} mode={this.state.mode} />
        <div className="ui grid">
          <div className="ui row">
            <div className="eleven wide column">
              <VideoDetail
                mode={this.state.mode}
                video={this.state.selectedVideo}
              />
            </div>
            <div className="five wide column">
              <VideoList
                mode={this.state.mode}
                videos={this.state.videos}
                onVideoSelect={this.onVideoSelect}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
