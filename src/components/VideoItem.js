import './VideoItem.css';
import PropTypes from 'prop-types';

import React from 'react';

const VideoItem = ({ video, onVideoSelect, mode }) => {
  const modeStyle = {
    night: {
      backgroundColor: '#3A3C40',
      color: '#fff'
    },
    day: {
      backgroundColor: '#fff',
      color: '#000000'
    }
  };

  return (
    <div
      className="video-item item"
      onClick={() => {
        onVideoSelect(video);
      }}
    >
      <img
        src={video.snippet.thumbnails.medium.url}
        alt={video.snippet.description}
        className="ui image"
      />
      <div className="content">
        <div className="header" style={{ color: modeStyle[mode].color }}>
          {video.snippet.title}
        </div>
      </div>
    </div>
  );
};

VideoItem.propTypes = {
  onVideoSelect: PropTypes.func.isRequired
};

export default VideoItem;
