import axios from 'axios';

const KEY = 'AIzaSyCqWoBANK5DI7-RO8noN-VyI65bsPshy5Q';

export default axios.create({
  baseURL: 'https://www.googleapis.com/youtube/v3',
  params: {
    part: 'snippet',
    maxResults: 5,
    key: KEY
  }
});
